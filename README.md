Md5Challenge TODO:

- dump a file called, FOUND in the AppGlobals.FOUND_OUTPUT_FOLDER

- from the FRONTEND, don't send another fileContentPage Job to the BACKEND, until a Job has been completed

- BACKEND: log how many jobs the BACKEND node has taken on, and how many it has completed so far
   - example: BACKEND(-1052121122):: Job #395 Complete | Job Load 53 | Peak Load 114 | Total characters now searched: 36133137
   - I should be able to to this with AtomicInteger.getAndIncrement(), and AtomicInteger.getAndDecrement()

- send a message to other backend nodes, when one backend node has completed the challenge
   - this would allow them to also display the success message

- currently the FRONTEND node sends all the data to the BACKEND router without regard for whether or not a backend node
  has finished its current task.
     - ISSUE #1: This probably causes ineffective RAM usage on the FRONTEND node
     - ISSUE #2: This probably causes ineffective HDD usage, as all of these will be cached to disk in "aeron_<id>" folders

- improve serialization beyond default java serializer, as noted in this message:
   [WARN] [SECURITY][08/10/2018 17:59:28.567] [ClusterSystem-akka.remote.default-remote-dispatcher-4] [akka.serialization.Serialization(akka://ClusterSystem)] Using the default Java serializer for class [com.whitehotlogic.mrclustoffelees.Md5Challenge.BackendMd5Result] which is not recommended because of performance implications. Use another serializer or disable this warning using the setting 'akka.actor.warn-about-java-serializer-usage'
   - i can use Avro for this

- create challenge-win alert system
   - TTS
        - do it with this: https://elinux.org/RPi_Text_to_Speech_(Speech_Synthesis)
        - example outputs:
            - while computing "fuck fuck fuck fuck fuck fuck"
            - after a win "i win, i win, i win, i win"
   - winning music track
   - LED array