package com.whitehotlogic.mrclustoffelees.Md5Challenge;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.TimeUnit;
import akka.actor.AbstractActor;
import akka.actor.ActorRef;
import akka.actor.ReceiveTimeout;
import akka.event.Logging;
import akka.event.LoggingAdapter;
import akka.routing.FromConfig;
import scala.concurrent.duration.Duration;
import com.whitehotlogic.mrclustoffelees.AppGlobals;

public class FrontendMd5Challenge extends AbstractActor
{
    final boolean _repeat;

    private int _backendJobsCompleted = 0;
    private int _backendJobsSent = 0;

    // create a stopwatch functionality so we can determine the elapsed time when the challenge is completed
    private static Instant _stopWatchStarted;

    // randomized file list of all folders in folder paths list
    ArrayList<Path> _randomizedFilePaths = new ArrayList<>();

    // currently unused (using System.out instead)
    LoggingAdapter log = Logging.getLogger(getContext().system(), this);

    // get the ActorRef for the BackendRouter
    private ActorRef _backendRouter = getContext().actorOf(FromConfig.getInstance().props(), AppGlobals.Backend.AKKA_ACTOR_REF);

    public FrontendMd5Challenge(boolean repeat)
    {
        _repeat = repeat;

        // start the stopWatch on instantiation of this class
        _stopWatchStarted = Instant.now();
    }

    @Override
    public void preStart()
    {
        sendJobs();

        getContext().setReceiveTimeout(Duration.create(AppGlobals.AKKA_RECEIVE_TIMEOUT, TimeUnit.SECONDS));
    }

    @Override
    public Receive createReceive()
    {
        return receiveBuilder()
                .match(BackendMd5Result.class, result ->
                {
                    // helpful for debugging messaging from backend (leave this here)
                    //System.out.println("FRONTEND:: Received message from backendId, '" +
                    //        result.BackendId + "': " + result + ".");
                    _backendJobsCompleted++;

                    if (result.FoundIt && result.TheFoundTextCollision != "")
                    {
                        // stop the stopwatch and store the elapsed time
                        java.time.Duration elapsedTime = java.time.Duration.between(_stopWatchStarted, Instant.now());

                        // shout out about the success
                        AppGlobals.Md5Challenge.SuccessMessage(result.TheFoundTextCollision, elapsedTime, result.BackendId);

                        // exit this program execution with success code
                        System.exit(0);
                    }

                    // DON'T EVER REPEAT
                    //if (_repeat)
                    //    sendJobs();

                    // DON'T EVER call .stop() or the FRONTEND WON'T WAIT for messages from the BACKEND
                    //   it causes all messages from the backend to just disappear (when using pipe)
                    //   or just disappear (if using sender().tell)
                    //
                    //else
                    //    getContext().stop(self());
                })
                .match(ReceiveTimeout.class, message ->
                {
                    System.out.println("FRONTEND: Receive has timed out.");

                    // DON'T RESEND JOBS ON TIMEOUT
                    //sendJobs();
                })
                .build();
    }

    void sendJobs()
    {
        System.out.println("FRONTEND:: Sending all jobs to Backend ActorRef, '" + AppGlobals.Backend.AKKA_ACTOR_REF + "'.");

        try
        {
            // iterate through all of the hardcoded root folders (allows specific folders)
            for (String folderPath: AppGlobals.Md5Challenge.ROOT_FOLDER_LIST) {

                _randomizedFilePaths.addAll(RandomizeFilePaths(folderPath));

                int totalFileCount = _randomizedFilePaths.size();

                for (int i = 0; i < totalFileCount; i++)
                {
                    // get the current file path using this loop index
                    Path filePath = _randomizedFilePaths.get(i);

                    // read text file into string
                    String wholeFile = new String(Files.readAllBytes(filePath));

                    // add paging since Akka max allowed message size is 262144 bytes
                    List<String> fileContentPages = splitEqually(wholeFile, AppGlobals.AKKA_MESSAGE_SIZE);

                    // send each page to the backend members
                    for (String fileContentPage : fileContentPages)
                    {
                        // this is should be the main job sending loop
                        //     (FRONTEND should not send a job to BACKEND unless the last one has been completed)
                        _backendRouter.tell(fileContentPage, self());
                        _backendJobsSent++;
                    }

                    fileContentPages.clear(); // help out the garbace collector and free up heap space

                    System.out.println("FRONTEND:: Finished sending file content to backend: " + filePath);

                    int currentFile = i + 1; // makes index accurate for human readability
                    System.out.println("FRONTEND:: Total files sent to backend: " + currentFile + " of " + totalFileCount);

                    // manually throttle the amount of jobs that are sent out
                    Thread.sleep(5000);
                }
            }
        }
        catch (IOException e)
        {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public static ArrayList<Path> RandomizeFilePaths(String folderPath) throws IOException
    {
        // create an object in which to store all file paths
        ArrayList<Path> fileList = new ArrayList<>();

        // recursively get all file paths
        Files.walk(Paths.get(folderPath))
                .filter(Files::isRegularFile)
                .filter(p -> p.getFileName().toString().endsWith(".txt")) // only want .txt files
                .forEach(file -> fileList.add(file));

        // randomize the file path list
        Collections.shuffle(fileList);

        return fileList;
    }

    public static List<String> splitEqually(String text, int size)
    {
        // Give the list the right capacity to start with.
        int initialCapacity = (text.length() + size - 1) / size;

        // create the array for holding pages of fileContent
        List<String> fileContentPages = new ArrayList<String>(initialCapacity);

        for (int start = 0; start < text.length(); start += size)
        {
            // used to add X characters to the front of the string
            //    - if we just splitEqually, we'd miss any strings that happen on the page border
            int offsetStart = 0;

            // only prefix the SEARCH_WINDOW_SIZE number characters to the string,
            //    if there are that many characters available
            if (start >= AppGlobals.Md5Challenge.SEARCH_WINDOW_SIZE)
                offsetStart = start - AppGlobals.Md5Challenge.SEARCH_WINDOW_SIZE;

            // create the fileContentPage
            String fileContentPage = text.substring(offsetStart, Math.min(text.length(), offsetStart + size));

            // remove illegal strings (should save on processing time in the backend)
            for (String illegalString : AppGlobals.Md5Challenge.ILLEGAL_STRINGS_LIST)
                fileContentPage.replace(illegalString, "");

            // add the fileContentPage to fileContentPages
            fileContentPages.add(fileContentPage);
        }

        return fileContentPages;
    }
}
