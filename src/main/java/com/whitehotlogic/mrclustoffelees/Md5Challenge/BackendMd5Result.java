package com.whitehotlogic.mrclustoffelees.Md5Challenge;

import java.io.Serializable;

public class BackendMd5Result implements Serializable
{
    private static final long serialVersionUID = 1L;

    public Boolean FoundIt = false;
    public String TheFoundTextCollision = "";

    public int BackendId;

    BackendMd5Result(boolean foundIt, String theFoundTextCollision, int backendId)
    {
        FoundIt = foundIt;
        TheFoundTextCollision = theFoundTextCollision;
        BackendId = backendId;
    }
}




