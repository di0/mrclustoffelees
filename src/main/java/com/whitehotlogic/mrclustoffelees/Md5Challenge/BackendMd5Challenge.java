package com.whitehotlogic.mrclustoffelees.Md5Challenge;

import akka.actor.AbstractActor;
import akka.actor.ActorRef;
import com.whitehotlogic.mrclustoffelees.AppGlobals;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.time.Duration;
import java.time.Instant;
import java.util.concurrent.CompletableFuture;

import static akka.pattern.PatternsCS.pipe;

public class BackendMd5Challenge extends AbstractActor
{
    private static int _totalCharactersNowSearched = 0;

    // not really sure i need this, but might help keep the backend from continuing to process things
    //     after it's already completed the challenge. This obviously
    private static boolean _found;

    // create a stopwatch functionality so we can determine the elapsed time when the challenge is completed
    private static Instant _stopWatchStarted;

    // create a unique BackendId for usage in logs and messaging
    private static int _backendID;
    private static String _backendLogPrefix;

    // global for this backend instance -- no need to keep processing once we've completed the challenge
    private static String _theFoundTextCollision;

    public BackendMd5Challenge()
    {
        // start the stopWatch on instantiation of this class
        _stopWatchStarted = Instant.now();

        // create the unique backendId
        _backendID = AppGlobals.Backend.CreateBackendId();

        // use the unique backendId to create the prefix used on all console logs
        _backendLogPrefix = AppGlobals.Backend.GetBackendLogPrefix(_backendID);
    }

    @Override
    public AbstractActor.Receive createReceive()
    {
        return receiveBuilder()
                .match(String.class, fileContentPage -> {

                    ActorRef sender = sender();

                    CompletableFuture<BackendMd5Result> result =
                            CompletableFuture.supplyAsync(() -> BackendMd5Challenge.ProcessJob(fileContentPage))
                                    .thenApply((textCollisionFound) ->
                                            new BackendMd5Result(textCollisionFound, _theFoundTextCollision, _backendID));

                    pipe(result, getContext().dispatcher()).to(sender());

                    // the two commented lines of code below are useful for debugging
                    //    the message flow.
                    //    sender.Tell() results in dead letters, whereas pipe does not,
                    //    but otherwise the functionality seems the same between the two
                    //BackendMd5Result theResult = result.get();
                    //sender.tell(theResult, self());
                })
                .build();
    }

    public static boolean ProcessJob(String fileContent)
    {
        // don't process anything if we've already completed the challenge
        if (!_found)
        {
            for (int index = 0; index < fileContent.length(); index++)
            {
                boolean md5CollisionFound = Md5CollisionSearch(AppGlobals.Md5Challenge.SEARCH_WINDOW_SIZE, index, fileContent.toCharArray());

                if (md5CollisionFound)
                    _found = true;
            }

            _totalCharactersNowSearched += fileContent.length();

            // prints on backend console to indicate progress
            System.out.println(_backendLogPrefix + "(Job Complete) Total characters now searched: " + _totalCharactersNowSearched);
        }

        return _found;
    }

    public static boolean Md5CollisionSearch(int searchWindowSize, int startingIndex, char[] dataToSearch)
    {
        String searchWindow = "";

        for (int forwardMovement = 0;
             forwardMovement <= searchWindowSize && startingIndex + searchWindowSize < dataToSearch.length;
             forwardMovement++) {

            // modify the search window using trivial search forward methodology
            searchWindow += dataToSearch[startingIndex + forwardMovement];

            if (Md5CollisionTest(searchWindow))
            {
                // stop the stopwatch and store the elapsed time
                Duration elapsedTime = Duration.between(_stopWatchStarted, Instant.now());

                // set the _theFoundTextCollision to the actual text
                _theFoundTextCollision = searchWindow;

                // shout out about the success
                AppGlobals.Md5Challenge.SuccessMessage(_theFoundTextCollision, elapsedTime, _backendID);

                return true;
            }
        }

        return false;
    }

    public static boolean Md5CollisionTest(String searchAttemptValue)
    {
        try
        {
            // create an instance of the Md5Challenge tool
            MessageDigest messageDigest = MessageDigest.getInstance("MD5");

            // create the Md5Challenge of the string as a byte array
            byte[] digest = messageDigest.digest(searchAttemptValue.getBytes());

            // convert the byte array to a number
            BigInteger number = new BigInteger(1, digest);

            // convert the number to a string
            String searchBufferMd5 = number.toString(16);

            // find out if it worked
            if (searchBufferMd5.equalsIgnoreCase(AppGlobals.Md5Challenge.THE_MD5_TO_FIND))
                return true;
        }
        catch (NoSuchAlgorithmException e)
        {
            System.out.println("NoSuchAlgorithmException: " + e);
        }

        return false;
    }
}
