package com.whitehotlogic.mrclustoffelees;

import java.io.IOException;

public class App
{
    public static void main(String[] args)
    {
        System.out.println("FRONTEND:: Mr. Clustoffelees is ready...");
        System.out.println("FRONTEND:: Press ENTER to Start...");

        // wait for enter key to start
        try
        {
            System.in.read();
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }

        System.out.println("FRONTEND:: Starting!");

        // FIRST, start all BACKEND nodes in the cluster
        BackendMain.main(new String[]{"2551", ChallengeType.Md5Challenge.toString()});
        BackendMain.main(new String[]{"2552", ChallengeType.Md5Challenge.toString()});
        BackendMain.main(new String[]{"0", ChallengeType.Md5Challenge.toString()});

        // SECOND, start the one and only FRONTEND node (after backend nodes have completely started)
        FrontendMain.main(new String[]{"0", ChallengeType.Md5Challenge.toString()});
    }
}
