package com.whitehotlogic.mrclustoffelees;

import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;
import akka.actor.ActorSystem;
import akka.actor.Props;
import com.whitehotlogic.mrclustoffelees.Md5Challenge.BackendMd5Challenge;

public class BackendMain
{
    public static void main(String[] args)
    {
        System.out.println("BACKEND:: Starting!");

        // Override the configuration of the port when specified as program argument
        final String port = args.length > 0 ? args[0] : "0";

        // if the challenge type is specified, then use it
        final ChallengeType challengeType = args.length > 1 ? ChallengeType.valueOf(args[1]) : ChallengeType.Md5Challenge;

        final Config config =
                ConfigFactory.parseString(
                        "akka.remote.netty.tcp.port=" + port + "\n" +
                                "akka.remote.artery.canonical.port=" + port).
                        withFallback(ConfigFactory.parseString("akka.cluster.roles = [backend]")).
                        withFallback(ConfigFactory.load(AppGlobals.AKKA_CONF_FILE));

        ActorSystem system = ActorSystem.create(AppGlobals.AKKA_ACTOR_SYSTEM, config);

        // this was created to make the class extensible to different ChallengeTypes
        switch (challengeType)
        {
            case Md5Challenge:
            {
                system.actorOf(Props.create(BackendMd5Challenge.class), AppGlobals.Backend.AKKA_ACTOR);
                break;
            }
        }

        system.actorOf(Props.create(MetricsListener.class), "metricsListener");
    }
}