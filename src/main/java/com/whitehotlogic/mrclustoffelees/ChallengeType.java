package com.whitehotlogic.mrclustoffelees;

public enum ChallengeType
{
    Md5Challenge,
    OtherChallenge1,
    OtherChallenge2
}
