package com.whitehotlogic.mrclustoffelees;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Random;

public class AppGlobals
{
    public final static boolean DEBUG = true; // currently unused
    public final static int MAX_THREAD_LIMIT = 10; // currently unused

    public final static int AKKA_RECEIVE_TIMEOUT = 10; // seconds
    public final static int AKKA_MESSAGE_SIZE = 10000; // bytes; Akka max message size is 262144 bytes
    public final static String AKKA_CONF_FILE = "mrclustoffelees";
    public final static String AKKA_ACTOR_SYSTEM = "ClusterSystem";

    public static class Frontend
    {
        public final static String AKKA_ACTOR = "Frontend";
    }

    public static class Backend
    {
        public final static String AKKA_ACTOR = "Backend";
        public final static String AKKA_ACTOR_REF = "BackendRouter";

        public final static String GetBackendLogPrefix(int backendID)
        {
            // show it in all BACKEND log messages
            return "BACKEND(" + backendID + "):: ";
        }

        public final static int CreateBackendId()
        {
            // pick a random backend ID
            // TODO: it would be better if this was the port number, or a combo of the machine name
            Random rand = new Random();
            return rand.nextInt();
        }
    }

    //
    // break this up into a nested class Challenges>>MD5, Challenges>>Pi (should shadow ChallengeType enum)
    //
    public static class Md5Challenge
    {
        // Your Hash: a62728123eecd56a817b64747bac32b9
        // Your String: and is a neglect in order
        // character count: 717337
        //public final static String THE_STRING_TO_FIND = "and is a neglect in order";


        public final static String THE_MD5_TO_FIND = "a62728123eecd56a817b64747bac32b9";
        public final static int SEARCH_WINDOW_SIZE = 25; // agreed for first match

        public final static List<String> ROOT_FOLDER_LIST = Arrays.asList("c:\\temp\\testFolder");
        public final static String SUCCESS_FILE_PATH = "c:\\Users\\dujohnson\\Desktop\\SUCCESS.txt";

        //public final static List<String> ROOT_FOLDER_LIST = Arrays.asList("e:");
        //public final static String SUCCESS_FILE_PATH = "~/Desktop/SUCCESS.txt";

        public final static List<String> ILLEGAL_STRINGS_LIST = Arrays.asList("\r\n");

        // shout about the success to console and to file
        public static void SuccessMessage(String theFoundTextCollision, Duration elapsedTime, int backendId)
        {
            // build the success message
            StringBuilder successMessage = new StringBuilder();
            successMessage.append("\r\n\r\n\r\n\r\n\r\n");
            successMessage.append("===================================================================================\r\n");
            successMessage.append("===================================================================================\r\n");
            successMessage.append("===================================================================================\r\n");
            successMessage.append("                 ___ _           _         __  __      _                \r\n");
            successMessage.append("  /\\/\\  _ __    / __\\ |_   _ ___| |_ ___  / _|/ _| ___| | ___  ___  ___ \r\n");
            successMessage.append(" /    \\| '__|  / /  | | | | / __| __/ _ \\| |_| |_ / _ \\ |/ _ \\/ _ \\/ __|\r\n");
            successMessage.append("/ /\\/\\ \\ |_   / /___| | |_| \\__ \\ || (_) |  _|  _|  __/ |  __/  __/\\__ \\\r\n");
            successMessage.append("\\/    \\/_(_)  \\____/|_|\\__,_|___/\\__\\___/|_| |_|  \\___|_|\\___|\\___||___/\r\n");
            successMessage.append("\r\n\r\n");
            successMessage.append("FOUND DAT COLLISION: " + "\"" + theFoundTextCollision + "\"\r\n");
            successMessage.append("\r\n");
            successMessage.append("Completed at: " + new SimpleDateFormat("yyyy.MM.dd HH:mm:ss").format(new Date()) + "\r\n");
            successMessage.append("\r\n");
            successMessage.append("Elapsed time: " + elapsedTime.toMinutes() + " minutes.\r\n");
            successMessage.append("\r\n");
            successMessage.append("Found by BackendId, '" + backendId + "'.\r\n");
            successMessage.append("===================================================================================\r\n");
            successMessage.append("===================================================================================\r\n");
            successMessage.append("===================================================================================\r\n");
            successMessage.append("\r\n\r\n\r\n\r\n\r\n");

            // write it to console
            System.out.println(successMessage.toString());

            // write it to file (because i might miss it on the console)
            Path path = Paths.get(Md5Challenge.SUCCESS_FILE_PATH);
            byte[] strToBytes = successMessage.toString().getBytes();
            try { Files.write(path, strToBytes); } catch (IOException e) { e.printStackTrace(); }

            // just exit here for now since this is the last thing any node should do
            System.exit(0);
        }
    }
}
