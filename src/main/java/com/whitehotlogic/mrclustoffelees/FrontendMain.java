package com.whitehotlogic.mrclustoffelees;

import java.util.concurrent.TimeUnit;

import com.whitehotlogic.mrclustoffelees.Md5Challenge.FrontendMd5Challenge;
import scala.concurrent.Await;
import scala.concurrent.duration.Duration;
import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;
import akka.actor.ActorSystem;
import akka.actor.Props;
import akka.cluster.Cluster;

public class FrontendMain
{
  public static void main(String[] args)
  {
    final Config config = ConfigFactory.parseString(
            "akka.cluster.roles = [frontend]").withFallback(
            ConfigFactory.load(AppGlobals.AKKA_CONF_FILE));

    final ActorSystem system = ActorSystem.create(AppGlobals.AKKA_ACTOR_SYSTEM, config);

    System.out.println("FRONTEND:: Mr. Clustoffelees will not start until there are at " +
                      "least 2 backend members in the cluster...");

    Cluster.get(system).registerOnMemberUp(new Runnable()
    {
      @Override
      public void run()
      {
        // if the challenge type is specified, then use it
        final ChallengeType challengeType = args.length > 1 ? ChallengeType.valueOf(args[1]) : ChallengeType.Md5Challenge;

        // this was created to make the class extensible to different ChallengeTypes
        switch (challengeType)
        {
          case Md5Challenge:
          {
            // the "args: false" prop tells the task not to repeat automatically
            system.actorOf(Props.create(FrontendMd5Challenge.class, false), AppGlobals.Frontend.AKKA_ACTOR);

            break;
          }
        }
      }
    });

    Cluster.get(system).registerOnMemberRemoved(new Runnable()
    {
      @Override
      public void run()
      {
        // exit JVM when ActorSystem has been terminated
        final Runnable exit = new Runnable()
        {
          @Override public void run()
          {
            System.exit(0);
          }
        };
        system.registerOnTermination(exit);

        // shut down ActorSystem
        system.terminate();

        // In case ActorSystem shutdown takes longer than 10 seconds,
        //   exit the JVM forcefully anyway. We must spawn a separate
        //   thread to not block current thread, since that would have
        //   blocked the shutdown of the ActorSystem.
        new Thread() {
          @Override public void run(){
            try
            {
              Await.ready(system.whenTerminated(), Duration.create(10, TimeUnit.SECONDS));
            }
            catch (Exception e)
            {
              System.exit(-1);
            }

          }
        }.start();
      }
    });
  }

}